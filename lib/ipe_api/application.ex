defmodule IpeAPI.Application do
  @moduledoc false

  use Application

  require Logger

  @impl Application
  @spec start(Application.start_type(), any) :: {:ok, pid} | {:ok, pid, Application.state()} | {:error, any}
  def start(_type, _args) do
    Logger.notice("starting")

    children = [
      IpeAPI.Web.Endpoint,
      IpeAPI.Repo
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: IpeAPI.Supervisor)
  end
end
