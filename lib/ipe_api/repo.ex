defmodule IpeAPI.Repo do
  @moduledoc """
  Primary PostgreSQL repository.
  """

  use Ecto.Repo, otp_app: :ipe_api, adapter: Ecto.Adapters.Postgres
end
