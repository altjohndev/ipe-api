defmodule IpeAPI.Web.Endpoint do
  @moduledoc false

  use Phoenix.Endpoint, otp_app: :ipe_api

  plug Plug.RequestId
  plug Plug.Parsers, parsers: [:json], pass: ["*/*"], json_decoder: Phoenix.json_library()
  plug Plug.Head
  plug IpeAPI.Web.Router
end
