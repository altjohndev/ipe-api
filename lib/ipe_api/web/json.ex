defmodule IpeAPI.Web.JSON do
  @moduledoc """
  JSON view for general renderings.
  """

  @doc """
  Renders data with optional errors.
  """
  @spec render_data(map, [String.t()]) :: map
  def render_data(data, errors \\ []) do
    %{data: data, errors: errors}
  end

  @doc """
  Renders errors with optional data.
  """
  @spec render_error(String.t() | [String.t()], map | nil) :: map
  def render_error(error_or_errors, data \\ nil) do
    %{data: data, errors: List.wrap(error_or_errors)}
  end
end
