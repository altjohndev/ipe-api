defmodule IpeAPI.Web.IPLocationJSON do
  @moduledoc """
  `IpeData.IPLocation` renderings.
  """

  alias IpeAPI.Web.JSON
  alias IpeData.IPLocation

  @doc """
  Renders `IpeData.IPLocation` data.

  Templates:

  - `one.json` - renders a single record
  """
  @spec render(String.t(), map) :: map
  def render("one.json", %{ip_location: %IPLocation{} = ip_location}) do
    JSON.render_data(%{ip_location: ip_location})
  end
end
