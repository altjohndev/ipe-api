defmodule IpeAPI.Web.ErrorJSON do
  @moduledoc """
  JSON view for error renderings.
  """

  alias IpeAPI.Web.JSON
  alias Phoenix.Controller

  @doc """
  With general template from Phoenix, renders error based on status message.
  """
  @spec render(String.t(), map) :: map
  def render(template, _assigns), do: template |> Controller.status_message_from_template() |> JSON.render_error()
end
