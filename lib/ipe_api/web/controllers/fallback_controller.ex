defmodule IpeAPI.Web.FallbackController do
  @moduledoc """
  Fallback controller to handle invalid results.
  """

  use Phoenix.Controller, formats: [:json]

  alias IpeAPI.Web.ErrorJSON
  alias Plug.Conn

  @doc """
  Handles invalid results.

  - `{:error, :not_found}` - Set status to `404` and renders `404` error content.
  """
  @spec call(Conn.t(), any) :: Conn.t()
  def call(%Conn{} = conn, result) do
    %Conn{} = conn = put_view(conn, json: ErrorJSON)

    case result do
      {:error, :not_found} -> conn |> put_status(404) |> render(:"404")
    end
  end
end
