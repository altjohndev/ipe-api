defmodule IpeAPI.Web.IPLocationController do
  @moduledoc """
  Controller for "/ip-locations" resources.
  """

  use Phoenix.Controller, formats: [:json]

  alias IpeAPI.Web.FallbackController
  alias IpeData.IPLocation
  alias IpeData.IPLocationReader
  alias Plug.Conn

  action_fallback FallbackController

  @doc """
  Fetches a single `IpeData.IPLocation` based on `ip_address`.
  """
  @spec show(Conn.t(), map) :: Conn.t()
  def show(%Conn{} = conn, %{"ip_address" => ip_address}) do
    with {:ok, %IPLocation{} = ip_location} <- IPLocationReader.fetch(ip_address) do
      render(conn, :one, ip_location: ip_location)
    end
  end
end
