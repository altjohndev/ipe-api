defmodule IpeAPI.Web.Router do
  @moduledoc false

  use Phoenix.Router

  alias IpeAPI.Web.IPLocationController

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/" do
    pipe_through :api
    resources "/ip-locations", IPLocationController, only: [:show], param: "ip_address"
  end
end
