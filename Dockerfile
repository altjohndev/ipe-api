FROM elixir:1.15.6-alpine

ENV MIX_ENV prod

RUN apk update && apk --no-cache --update add git openssh

RUN mix local.hex --force --if-missing && mix local.rebar --force --if-missing

COPY mix.exs mix.lock ./

RUN mix deps.get

COPY . .

RUN mix release ipe_api --overwrite

FROM alpine:3.18

ENV MIX_ENV prod

WORKDIR /opt/app

RUN apk update && apk --no-cache --update add openssl g++ ncurses-libs

COPY --from=0 _build/prod/rel/ipe_api .

CMD bin/ipe_api eval "IpeData.Migrator.migrate IpeAPI.Repo" && bin/ipe_api start
