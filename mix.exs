defmodule IpeAPI.MixProject do
  @moduledoc false

  use Mix.Project

  @source_url "https://gitlab.com/altjohndev/ipe-api"
  @version "0.0.1"

  def project do
    env = Mix.env()

    [
      app: :ipe_api,
      name: "Ipê API",
      description: "REST JSON API for Ipê services",
      elixir: "~> 1.15",
      version: @version,
      source_url: @source_url,
      deps: deps(env),
      aliases: aliases(),
      preferred_cli_env: preferred_cli_env(),
      elixirc_options: [warnings_as_errors: true],
      elixirc_paths: elixirc_paths(env),
      package: package(),
      docs: docs(),
      releases: releases(),
      test_coverage: test_coverage()
    ]
  end

  def application do
    [
      mod: {IpeAPI.Application, []},
      extra_applications: [:inets, :logger, :runtime_tools]
    ]
  end

  defp deps(env) do
    [
      {:credo, "~> 1.7.1", only: [:dev, :test], runtime: false},
      {:ecto, "~> 3.10.3"},
      {:ex_doc, "~> 0.30.6", only: [:dev, :test], runtime: false},
      {:ipe_data, git: "https://gitlab.com/altjohndev/ipe-data.git", tag: "v0.0.1", env: env},
      {:jason, "1.4.1"},
      {:phoenix, "1.7.9"},
      {:plug_cowboy, "2.6.1"},
      {:postgrex, "~> 0.17.3"},
      {:styler, "~> 0.9.6", only: [:dev, :test], runtime: false}
    ]
  end

  defp aliases do
    [
      "ecto.migrate": ["ipe_data.migrate IpeAPI.Repo"],
      "ecto.reset": ["ecto.drop", "ecto.create", "ecto.migrate"],
      "test.all": ["test.static", "test"],
      "test.ci": ["test.static", "ecto.reset", "test --trace --cover"],
      "test.static": ["format --check-formatted", "credo"]
    ]
  end

  defp preferred_cli_env do
    [
      "ecto.migrate": :test,
      "ecto.reset": :test,
      "test.all": :test,
      "test.ci": :test,
      "test.static": :test,
      test: :test
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_env), do: ["lib"]

  defp package do
    [
      licenses: ["MIT"],
      links: %{gitlab: @source_url}
    ]
  end

  defp docs do
    [
      source_ref: "v#{@version}",
      extras: ["README.md", "LICENSE"],
      main: "readme"
    ]
  end

  defp releases do
    [
      ipe_api: [
        include_executables_for: [:unix],
        applications: [runtime_tools: :permanent]
      ]
    ]
  end

  defp test_coverage do
    [
      summary: [threshold: 80],
      ignore_modules: [
        ~r/IpeAPI.Test.*/,
        IpeAPI.Repo,
        IpeAPI.Web.Router.Helpers
      ]
    ]
  end
end
