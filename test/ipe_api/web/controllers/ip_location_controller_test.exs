defmodule IpeAPI.Web.IPLocationControllerTest do
  @moduledoc """
  Test for resources from route `/ip-locations`, managed by `IpeAPI.Web.IPLocationController`
  """

  use ExUnit.Case, async: true

  import Phoenix.VerifiedRoutes, only: [sigil_p: 2]

  alias Ecto.Adapters.SQL.Sandbox
  alias IpeAPI.Repo
  alias IpeData.IPLocation
  alias IpeData.Test.Fake
  alias Plug.Conn

  require Phoenix.ConnTest, as: ConnTest

  @endpoint IpeAPI.Web.Endpoint
  @router IpeAPI.Web.Router

  describe "route /ip-locations/:ip_address" do
    test ":ok on known ip_address" do
      Sandbox.checkout(Repo)

      %IPLocation{
        ip_address: ip_address,
        country_code: country_code,
        country: country,
        city: city,
        latitude: latitude,
        longitude: longitude
      } = Fake.create(IPLocation)

      latitude = Decimal.to_string(latitude)
      longitude = Decimal.to_string(longitude)

      %Conn{status: 200} = conn = ConnTest.get(ConnTest.build_conn(), ~p"/ip-locations/#{ip_address}")

      assert %{
               "data" => %{
                 "ip_location" => %{
                   "ip_address" => ^ip_address,
                   "country_code" => ^country_code,
                   "country" => ^country,
                   "city" => ^city,
                   "latitude" => ^latitude,
                   "longitude" => ^longitude
                 }
               },
               "errors" => []
             } = Jason.decode!(conn.resp_body)
    end

    test ":not_found on unknown ip_address" do
      Sandbox.checkout(Repo)

      %Conn{status: 404} = conn = ConnTest.get(ConnTest.build_conn(), ~p"/ip-locations/#{Fake.ipv4()}")

      assert %{
               "data" => nil,
               "errors" => ["Not Found"]
             } = Jason.decode!(conn.resp_body)
    end
  end
end
