[
  import_deps: [:phoenix, :ecto, :ecto_sql],
  inputs: ["*.exs", "{config,lib,test,priv}/**/*.{ex,exs}"],
  plugins: [Styler],
  line_length: 120
]
