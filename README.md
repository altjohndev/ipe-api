[![pipeline status](https://gitlab.com/altjohndev/ipe-api/badges/main/pipeline.svg)](https://gitlab.com/altjohndev/ipe-api/-/commits/main)
[![coverage report](https://gitlab.com/altjohndev/ipe-api/badges/main/coverage.svg)](https://gitlab.com/altjohndev/ipe-api/-/commits/main)

# Ipê API

REST JSON API for [Ipê](https://gitlab.com/altjohndev/ipe) services.

## Deployment

Minimal `docker-compose.yml` configuration:

```yaml
version: "3"

services:
  api:
    image: altjohndev/ipe-api:v0.0.1
    environment:
      IPE_API_POSTGRES_URL: ecto://postgres:postgres@postgres:5432/ipe
    ports:
      - 8080:80
    depends_on:
      - postgres

  postgres:
    image: postgres:16.0
    environment:
      POSTGRES_DB: ipe
      POSTGRES_PASSWORD: postgres
    ports:
      - 5432:5432
```

Access through <http://localhost:8080>

## Routes

- `/ip-locations/:ip_address` - Fetch an IP Location record by a particular IPv4 address
