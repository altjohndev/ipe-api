import Config

config :ipe_api, IpeAPI.Repo,
  pool: Ecto.Adapters.SQL.Sandbox,
  url: System.get_env("IPE_API_POSTGRES_URL", "ecto://postgres:postgres@localhost:5432/ipe_api_test"),
  log: false

config :ipe_api, IpeAPI.Web.Endpoint, http: [port: 4001], server: true
