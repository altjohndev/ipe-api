import Config

config :ipe_api, ecto_repos: [IpeAPI.Repo]

config :ipe_api, IpeAPI.Web.Endpoint, render_errors: [view: IpeAPI.Web.ErrorJSON, accepts: [:json]]

config :ipe_data, default_repo: IpeAPI.Repo

import_config "#{config_env()}.exs"
