import Config

config :ipe_api, IpeAPI.Web.Endpoint,
  http: [port: 80, protocol_options: [idle_timeout: :timer.minutes(5)]],
  server: true

config :logger, level: :info, compile_time_purge_matching: [[level_lower_than: :info]]
