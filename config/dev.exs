import Config

config :ipe_api, IpeAPI.Repo,
  url: System.get_env("IPE_API_POSTGRES_URL", "ecto://postgres:postgres@localhost:5432/ipe_api_dev")

config :ipe_api, IpeAPI.Web.Endpoint,
  http: [port: 4000],
  check_origin: false,
  code_reloader: true,
  debug_errors: true,
  watchers: []

config :logger, level: :debug
config :phoenix, plug_init_mode: :runtime, stacktrace_depth: 20
