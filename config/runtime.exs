import Config

if config_env() == :prod do
  config :ipe_api, IpeAPI.Repo, url: System.fetch_env!("IPE_API_POSTGRES_URL")
end
